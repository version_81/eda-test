const server = (express())();
const db = connect_to_mongoDB();
const EventEmitter = require('events');
const appEvents = {
  createTranslation: 'create.translation',
};
const modules = ['User', 'Payment', 'Translation'];
const run = (modules) => {
  modules.forEach(module => module.run());
};

const router = (data) => {
  let { action, response, params } = data;

  const doListener = async (listener) => {
    return await listener(params) === false;
  };

  const listenersPrepare = EventEmitter.listeners(`${action}.PREPARE`);
  const listenersDone = EventEmitter.listeners(`${action}.DONE`);

  if (listenersPrepare.some(doListener(data))){
    response = 'ERROR';
    return false;
  }

  if (listenersDone.some(doListener)){
    response = 'ERROR';
    return false;
  }

  response = 'OKAY';
  return true;
};

class User{
  static run(){
    this.createTranslation();
  }

  static createTranslation(){
    EventEmitter.on(`${appEvents.createTranslation}.PREPARE`,  this.createTranslationPrepare());
    EventEmitter.on(`${appEvents.createTranslation}.DONE`, this.createTranslationDone());
  }

  static async createTranslationPrepare(params){
    const { userId } = params;

    const user = db.getUser(userId);

    if (!user || user.group !== 'agent') {
      return false;
    }

    return true;
  }

  static async createTranslationDone(params){
    const { userId } = params;

    db.update({countOfTranslation: +1}, { user });

    return true;
  }
}

const PaymentService = require('stripeOrAny');
class Payment{
  static run(){
    this.createTranslation();
  }

  static createTranslation(){
    EventEmitter.on(`${appEvents.createTranslation}.PREPARE`,  this.createTranslationPrepare);
    EventEmitter.on(`${appEvents.createTranslation}.DONE`, this.createTranslationDone);
  }

  static async createTranslationPrepare(params){
    const { paymentCard } = params;

    if (!PaymentService.checkCard(paymentCard)) {
      return false;
    }

    if (!PaymentService.checkBalanse(paymentCard)) {
      return false;
    }

    return true;
  }

  static async createTranslationDone(params){
    const { paymentCard } = params;

    if (!PaymentService.doCharge(paymentCard)) {
      return false;
    }

    return true;
  }
}

class Translation{
  static run(){
    this.createTranslation();
  }

  static createTranslation(){
    EventEmitter.on(`${appEvents.createTranslation}.PREPARE`, this.createTranslationDone());
  }

  static async createTranslationDone(params){
    db.create({
      translation,
    });

    return true;
  }
}

run(modules);
server.get('all_routers_by_graphQL', router);


server.listen(3000, () => console.log('Server is running'));



// еще один вариант роутера
// actionId = индификатор события, можно заменить на eventObj.
// error и response внутри модулей
// если что, можно заюзать транзакции
const router2 = () => {
  let { action, response, params } = data;

  const actionId = `${action}${+new Date()}`;
  const countOflistener = EventEmitter.listenerCount(action);

  let countOfResponse = 0;

  EventEmitter.on(action, () => {
    countOfResponse += 1;
    if (countOfResponse === countOflistener) {
      EventEmitter.off(actionId);
      EventEmitter.on(`${action}.DONE`, { actionId, params });
    }
  });

  EventEmitter.emit(`${actionId}.PREPARE`, { actionId, params });

  return true;
}